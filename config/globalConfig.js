module.exports.server = {
  host: 'localhost',
  port: 5555
};


module.exports.video_status = {
  processing: 'processing',
  done: 'done',
  scheduled: 'scheduled',
  failed: 'failed',
}