module.exports = {
    'no_token_provided': { text: 'No token provided', code: 'NO_TOKEN_PROVIDED' },
    'expired_token': { text: 'Token expired', code: 'TOKEN_EXPIRED' },
    'invalid_token': { text: 'Invalid token', code: 'INVALID_TOKEN' },
    'time_range_required': { text: 'Set start time and end time in seconds for cutting', code: 'TIME_RANGE_REQUIRED' },
    'video_id_required': { text: 'Video id required', code: 'VIDEO_ID_REQUIRED' },
    'no_video_found': { text: 'No video found', code: 'NO_VIDEO_FOUND' },
};