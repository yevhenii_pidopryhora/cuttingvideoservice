'use strict';

const helpersModule = require('../controller/helpers');
const messagesList = require('../config/messagesList');


/**
 * Set headers
 * @param req Request
 * @param res Response
 * @param next Callback
 */
module.exports.setHeaders = (req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token, x-locale');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    if ('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
};


/**
 * No router provider callback
 * @param req Request
 * @param res Response
 */
module.exports.errorNotFound = (req, res) => {
    res.status(404).json({
        status: false,
        error: 'No method provided',
    });
};


module.exports.checkAccessToken = async (req, res, next) => {
    try {
        // check header, url parameters or post parameters for token
        const token = req.body.token || req.query.token || req.headers['x-access-token'] || null;

        if (!token) {
            res.status(401).send({
                status: false,
                error: messagesList.no_token_provided
            });
        } else {
            // verifies secret and checks exp
            const decodeToken = await helpersModule.verifyUserToken(token);
            
            // if isset error
            if (decodeToken.message) {
                let errorMessage = {};
                switch (decodeToken.message) {
                    case 'jwt expired':
                        errorMessage = messagesList.expired_token;
                        break;

                    default:
                        errorMessage = messagesList.invalid_token;
                        break;
                }

                res.status(401).send({
                    status: false,
                    error: errorMessage
                });
            } else {
                // store User data
                req.user = decodeToken;
                next();
            }
        }
    } catch (error) {
        res.status(401).send({
            status: false,
            error: error.message
        });
    }
};