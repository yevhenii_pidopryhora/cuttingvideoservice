const express = require('express');
const routesHandlers = require('./handlers');

// routes
const authRoutes = require('./auth');
const videoRoutes = require('./video');

const router = new express.Router();

router.use('/auth', authRoutes);
router.use('/video', routesHandlers.checkAccessToken, videoRoutes);

module.exports = router;

