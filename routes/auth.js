const express = require('express');
const AuthController = require('../controller/AuthController');

const router = new express.Router();


/**
 * @api {post} /api/auth/create_user Create user
 * @apiName CreateUser
 * @apiGroup Auth
 * @apiDescription Create user
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true,
 *       "data": "...token"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "error": "...error"
 *     }
 */
router.post('/create_user', AuthController.createUser);


module.exports = router;