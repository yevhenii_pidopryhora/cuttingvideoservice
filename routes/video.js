const express = require('express');
const VideoController = require('../controller/VideoController');

const router = new express.Router();


/**
 * @api {post} /api/video/get_list_videos 1. Get list videos for user
 * @apiName GetListVides
 * @apiGroup Video
 * @apiDescription Get list videos for user
 * 
 * @apiHeader {String} x-access-token User token.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true,
 *       "data": [
 *           {
 *               "_id": "5be3fe7b1a4852498be92f16",
 *               "video_link": "http://localhost:5555/video/5be2aef1597ae91258558118_20181108_111435.mp4",
 *               "input_duration": "140",
 *               "output_duration": "30",
 *               "status": "done",                
 *           },
 *           {
 *               "_id": "5be3fe7b1a1112498be92f16",
 *               "video_link": "http://localhost:5555/video/5be2aef1597ae91258558118_20181108_222435.mp4",
 *               "input_duration": "123",
 *               "output_duration": "50",
 *               "status": "done",                
 *           }
 *       ]
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "error": "...error"
 *     }
 */
router.post('/get_list_videos', VideoController.getListVideos);


/**
 * @api {post} /api/video/cut_video 2. Upload video
 * @apiName UploadVideo
 * @apiGroup Video
 * @apiDescription Upload video
 * 
 * @apiHeader {String} x-access-token User token.
 * 
 * @apiParam {String} start_time Start time in seconds for cutting
 * @apiParam {String} end_time End time in seconds for cutting
 * @apiParam {File} video User video
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true,
 *       "data": "Processing"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "error": "...error"
 *     }
 */
router.post('/cut_video', VideoController.cutVideo);


/**
 * @api {post} /api/video/restart_video_cutting 3. Restart video cutting
 * @apiName RestartVideoCutting
 * @apiGroup Video
 * @apiDescription Restart video cutting
 * 
 * @apiHeader {String} x-access-token User token.
 * 
 * @apiParam {String} video_id Video id
 * 
* @apiParamExample {json} Request-Example:
 *      { 
 *          video_id: 5be2ff33cd936b02f9ecbd23
 *      }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true,
 *       "data": "Processing"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "error": "...error"
 *     }
 */
router.post('/restart_video_cutting', VideoController.restartVideoCutting);


/**
 * @api {post} /api/video/delete_video 4. Delete video
 * @apiName DeleteVideo
 * @apiGroup Video
 * @apiDescription Delete video
 * 
 * @apiHeader {String} x-access-token User token.
 * 
 * @apiParam {String} video_id Video id
 * 
* @apiParamExample {json} Request-Example:
 *      { 
 *          video_id: 5be2ff33cd936b02f9ecbd23
 *      }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true,
 *       "data": "Deleted"
 *     }
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": false,
 *       "error": "...error"
 *     }
 */
router.post('/delete_video', VideoController.deleteVideo);


module.exports = router;