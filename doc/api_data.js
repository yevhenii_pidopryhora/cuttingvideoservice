define({ "api": [
  {
    "type": "post",
    "url": "/api/auth/create_user",
    "title": "Create user",
    "name": "CreateUser",
    "group": "Auth",
    "description": "<p>Create user</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": \"...token\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"error\": \"...error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/auth.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/video/delete_video",
    "title": "4. Delete video",
    "name": "DeleteVideo",
    "group": "Video",
    "description": "<p>Delete video</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "video_id",
            "description": "<p>Video id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{ \n    video_id: 5be2ff33cd936b02f9ecbd23\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": \"Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"error\": \"...error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "post",
    "url": "/api/video/get_list_videos",
    "title": "1. Get list videos for user",
    "name": "GetListVides",
    "group": "Video",
    "description": "<p>Get list videos for user</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"_id\": \"5be3fe7b1a4852498be92f16\",\n          \"video_link\": \"http://localhost:5555/video/5be2aef1597ae91258558118_20181108_111435.mp4\",\n          \"input_duration\": \"140\",\n          \"output_duration\": \"30\",\n          \"status\": \"done\",                \n      },\n      {\n          \"_id\": \"5be3fe7b1a1112498be92f16\",\n          \"video_link\": \"http://localhost:5555/video/5be2aef1597ae91258558118_20181108_222435.mp4\",\n          \"input_duration\": \"123\",\n          \"output_duration\": \"50\",\n          \"status\": \"done\",                \n      }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"error\": \"...error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "post",
    "url": "/api/video/restart_video_cutting",
    "title": "3. Restart video cutting",
    "name": "RestartVideoCutting",
    "group": "Video",
    "description": "<p>Restart video cutting</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "video_id",
            "description": "<p>Video id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{ \n    video_id: 5be2ff33cd936b02f9ecbd23\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": \"Processing\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"error\": \"...error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/video.js",
    "groupTitle": "Video"
  },
  {
    "type": "post",
    "url": "/api/video/cut_video",
    "title": "2. Upload video",
    "name": "UploadVideo",
    "group": "Video",
    "description": "<p>Upload video</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User token.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "start_time",
            "description": "<p>Start time in seconds for cutting</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "end_time",
            "description": "<p>End time in seconds for cutting</p>"
          },
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "video",
            "description": "<p>User video</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": \"Processing\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": false,\n  \"error\": \"...error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/video.js",
    "groupTitle": "Video"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "_home_jeka_Studying_CuttingVideoService_doc_main_js",
    "groupTitle": "_home_jeka_Studying_CuttingVideoService_doc_main_js",
    "name": ""
  }
] });
