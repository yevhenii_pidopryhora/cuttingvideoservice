const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const globalConfig = require('./config/globalConfig');

const routesHandlers = require('./routes/handlers');
const routesList = require('./routes');

const app = express();
const port = process.env.PORT || globalConfig.server.port;
const http = require('http').Server(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


/**
 * Set up headers
 */
app.use(routesHandlers.setHeaders);


app.use(express.static(path.join(__dirname, 'public')));
app.use('/doc_for_api', express.static('doc'));

app.use('/api', routesList);


http.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

