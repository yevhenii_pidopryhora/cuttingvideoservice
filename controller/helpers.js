const jwt = require('jsonwebtoken');
const fs = require('fs');

const tokenPrivateKey = fs.readFileSync(__dirname + '/../config/keys/token_private.key', 'utf8');
const tokenPublicKey = fs.readFileSync(__dirname + '/../config/keys/token_public.pem', 'utf8');

/**
 * Handler before response sending
 * @param data
 * @returns {*}
 */
module.exports.responseHandler = (data) => {
    let response = {}
    if (data.error) {
        return {
            status: false,
            error: data.message
        };
    } else {
        response.status = true;
        response.data = data;
    }

    return response;
};


module.exports.generateUserToken = (userObject) => {
    return jwt.sign(userObject, tokenPrivateKey, {
        algorithm: 'RS256',
        expiresIn: '365d'
    });
}


module.exports.verifyUserToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, tokenPublicKey, (err, decoded) => {
            if (err) {
                resolve(err);
            } else {
                resolve(decoded);
            }
        });
    });
}


module.exports.verifyUserToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, tokenPublicKey, (err, decoded) => {
            if (err) {
                resolve(err);
            } else {
                resolve(decoded);
            }
        });
    });
}