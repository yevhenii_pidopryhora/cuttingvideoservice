const UserModel = require('../model/UserModel').User;
const helpersModule = require('./helpers');

exports.createUser = async (req, res) => {
    try {

        const newUser = new UserModel();

        newUser.save((error, savedUser) => {
            if (error) {
                res.json(helpersModule.responseHandler({ error: 1, message: error }));
            } else {
                const token = helpersModule.generateUserToken({
                    user_id: savedUser._id
                });

                savedUser.token = token;
                savedUser.save((err, savedUser) => {
                    if (err) {
                        res.json(helpersModule.responseHandler({ error: 1, message: err }));
                    } else {
                        res.json(helpersModule.responseHandler(token));
                    }
                });
            }
        });
    } catch (error) {
        console.log(error);
        res.json(helpersModule.responseHandler({ error: 1, message: error.message }));
    }
};