const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const { getVideoDurationInSeconds } = require('get-video-duration');
const momentTimezone = require('moment-timezone');
const VideoModel = require('../model/VideoModel').Video;
const helpersModule = require('./helpers');
const globalConfig = require('../config/globalConfig');
const messagesList = require('../config/messagesList');

async function cuttingVideoByTimeRange(videoLink, startTime, endTime, videoModel) {
    //get random cutting status for example
    const statuses = [globalConfig.video_status.done, globalConfig.video_status.failed];
    const cuttingStatus = statuses[Math.floor(Math.random() * statuses.length)];

    // emulate cutting during 1 minute
    setTimeout(async () => {
        //get input video duration
        const fullVideoLink = path.join(__dirname, '../public', videoLink);
        const output_duration = await getVideoDurationInSeconds(fullVideoLink);

        videoModel.status = cuttingStatus;
        videoModel.output_duration = parseInt(output_duration);

        videoModel.save((err, updatedVideo) => {
            if (err) {
                console.log(err);
            } else {
                console.log('OK');
            }
        });
    }, 60000);

    return true;
}


/**
 * get list videos for user
 */
exports.getListVideos = (req, res) => {
    try {
        VideoModel.find({ user_id: req.user.user_id }, (err, videos) => {
            if (err) {
                res.json(helpersModule.responseHandler({ error: 1, message: err }));
            } else {
                videos.map((video) => {
                    
                    video.video_link = `${req.protocol}://${req.headers.host}${video.video_link}`;
                    return video;
                });

                res.json(helpersModule.responseHandler(videos));
            }
        }).select({ 'video_link': 1, 'status': 1, 'input_duration': 1, 'output_duration': 1 });
    } catch (error) {
        console.log(error);
        res.json(helpersModule.responseHandler({ error: 1, message: error.message }));
    }
};


/**
 * save video and cut by time ranges
 */
exports.cutVideo = (req, res) => {

    const form = new formidable.IncomingForm();
    let startCuttingTime = 0;
    let endCuttingTime = 0;
    let videoLink = '';
    let fileFormat = '';

    form.multiples = true;
    form.parse(req);

    try {
        form.on('field', (fieldName, value) => {
            let formValue = value.trim();
            if (fieldName == 'start_time') {
                startCuttingTime = formValue;
            } else if (fieldName == 'end_time') {
                endCuttingTime = formValue;
            }
        }).on('file', (fieldName, file) => {
            fileFormat = file.name.split('.');
            fileFormat = fileFormat[fileFormat.length - 1];

            form.uploadDir = path.join(__dirname, '../public/video/');

            let newFilename = `${req.user.user_id}_${momentTimezone().format('YYYYMMDD_HHmmss')}.${fileFormat}`;

            fs.rename(file.path, form.uploadDir + newFilename, function (e) {
                if (e) {
                    console.error(e);
                }
            });
            filePath = path.join(form.uploadDir, newFilename);
            videoLink = `/video/${newFilename}`;
        }).on('error', function (err) {
            return done(err, null);
        }).on('end', async () => {

            if (!startCuttingTime || !endCuttingTime) {
                res.json(helpersModule.responseHandler({ error: 1, message: messagesList.time_range_required }));
            } else {
                //get input video duration
                const fullVideoLink = path.join(__dirname, '../public', videoLink);
                const input_video_duration = await getVideoDurationInSeconds(fullVideoLink)

                const videoStatus = globalConfig.video_status.processing;

                const newVideoForUser = new VideoModel();

                // add data for save
                newVideoForUser.user_id = req.user.user_id;
                newVideoForUser.video_link = videoLink;
                newVideoForUser.start_cutting_time = startCuttingTime;
                newVideoForUser.end_cutting_time = endCuttingTime;
                newVideoForUser.input_duration = parseInt(input_video_duration);
                newVideoForUser.status = videoStatus;

                newVideoForUser.save((error, savedVideo) => {
                    if (error) {
                        console.log(error);
                        res.json(helpersModule.responseHandler({ error: 1, message: error }));
                    } else {
                        cuttingVideoByTimeRange(videoLink, startCuttingTime, endCuttingTime, savedVideo);
                    }
                });

                res.json(helpersModule.responseHandler(videoStatus[0].toUpperCase() + videoStatus.slice(1)));
            }
        });
    } catch (error) {
        console.log(error);
        res.json(helpersModule.responseHandler({ error: 1, message: error.message }));
    }
};


/**
 * restart video cutting
 */
exports.restartVideoCutting = (req, res) => {
    try {
        const { video_id } = req.body;

        if (!video_id) {
            res.json(helpersModule.responseHandler({ error: 1, message: messagesList.video_id_required }));
        }

        VideoModel.findOne({ _id: video_id, status: globalConfig.video_status.failed }, (err, video) => {
            if (err) {
                res.json(helpersModule.responseHandler({ error: 1, message: err }));
            } else {
                if (!video) {
                    res.json(helpersModule.responseHandler({ error: 1, message: messagesList.no_video_found }));
                } else {
                    // update status - in process
                    video.status = globalConfig.video_status.processing;

                    video.save((err, updatedVideo) => {
                        if (err) {
                            console.log(err);
                            res.json(helpersModule.responseHandler({ error: 1, message: err }));
                        } else {
                            console.log('OK');
                        }
                    });

                    // run function on cutting
                    cuttingVideoByTimeRange(video.video_link, video.start_cutting_time, video.end_cutting_time, video);

                    const videoStatus = globalConfig.video_status.processing;
                    res.json(helpersModule.responseHandler(videoStatus[0].toUpperCase() + videoStatus.slice(1)));
                }
            }
        }).select({ 'video_link': 1, 'start_cutting_time': 1, 'end_cutting_time': 1 });
    } catch (error) {
        console.log(error);
        res.json(helpersModule.responseHandler({ error: 1, message: error.message }));
    }
};


/**
 * delete video
 */
exports.deleteVideo = (req, res) => {
    try {
        let { video_id } = req.body;

        if (!video_id) {
            res.json(helpersModule.responseHandler({ error: 1, message: messagesList.video_id_required }));
        }

        const objVideoId = new ObjectId(video_id);

        VideoModel.findById(objVideoId, (err, video) => {
            if (err) {
                console.log(err);
                res.json(helpersModule.responseHandler({ error: 1, message: messagesList.no_video_found }));
            } else {
                if (!video) {
                    res.json(helpersModule.responseHandler({ error: 1, message: messagesList.no_video_found }));
                } else {

                    // delete video from db and server
                    const fullVideoLink = path.join(__dirname, '../public', video.video_link);
                    VideoModel.findByIdAndRemove(objVideoId, (err, updatedVideo) => {
                        if (err) {
                            console.log(err);
                            res.json(helpersModule.responseHandler({ error: 1, message: err }));
                        } else {                    //         
                            fs.unlinkSync(fullVideoLink);
                            res.json(helpersModule.responseHandler('Deleted'));
                        }
                    });
                }
            }
        }).select({ 'video_link': 1, 'start_cutting_time': 1, 'end_cutting_time': 1 });
    } catch (error) {
        console.log(error);
        res.json(helpersModule.responseHandler({ error: 1, message: error.message }));
    }
};