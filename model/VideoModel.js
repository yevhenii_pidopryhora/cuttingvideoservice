const mongoose = require('mongoose');
const dbConfig = require('../config/dbConfig');

mongoose.Promise = global.Promise;
const mongooseConnection = mongoose.createConnection(dbConfig.dbUrl, { useNewUrlParser: true });


const VideoSchema = mongoose.Schema({
  user_id: String,
  video_link: String,
  start_cutting_time: String,
  end_cutting_time: String,
  input_duration: String,
  output_duration: String,
  status: String,
});


exports.Video = mongooseConnection.model('Video', VideoSchema);
