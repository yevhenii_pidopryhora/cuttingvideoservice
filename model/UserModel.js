const mongoose = require('mongoose');
const dbConfig = require('../config/dbConfig');

mongoose.Promise = global.Promise;
const mongooseConnection = mongoose.createConnection(dbConfig.dbUrl, { useNewUrlParser: true });

const UserSchema = mongoose.Schema({
  username: String,
  token: String,  
});

exports.User = mongooseConnection.model('User', UserSchema);
